-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 01-11-2018 a las 18:58:08
-- Versión del servidor: 5.7.24-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tfgamaza`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerts_levels`
--

CREATE TABLE `alerts_levels` (
  `id` int(4) NOT NULL,
  `level` int(4) NOT NULL,
  `property` varchar(128) NOT NULL,
  `advices` text NOT NULL,
  `notification_color` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alerts_levels`
--

INSERT INTO `alerts_levels` (`id`, `level`, `property`, `advices`, `notification_color`) VALUES
(1, 1, 'Air', 'Good level, indications: ...', '#00e400'),
(2, 2, 'Air', 'Moderate level, indications: ...', '#ffff00'),
(3, 3, 'Air', 'Unhealthy level for sensitive groups, indications: ...', '#ff7e00'),
(4, 4, 'Air', 'Unhealthy level, indications: ...', '#ff0000'),
(5, 5, 'Air', 'Very Unhealthy level, indications: ...', '#8f3f97'),
(6, 6, 'Air', 'Hazardous level, indications: ...', '#7e0023'),
(7, 1, 'Water', 'Water Advices 1', '#00e400'),
(8, 2, 'Water', 'Water Advices 2', '#ff7e00'),
(9, 3, 'Water', 'Water Advices 3', '#ff0000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerts_levels_users`
--

CREATE TABLE `alerts_levels_users` (
  `id` int(4) NOT NULL,
  `level_id` int(4) NOT NULL,
  `user_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alerts_levels_users`
--

INSERT INTO `alerts_levels_users` (`id`, `level_id`, `user_id`) VALUES
(19, 6, 1),
(20, 9, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerts_users`
--

CREATE TABLE `alerts_users` (
  `id` int(4) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(128) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alerts_users`
--

INSERT INTO `alerts_users` (`id`, `name`, `email`, `is_enabled`) VALUES
(1, 'Ángel Gamaza', 'angel.gamaza@gmail.com', 0),
(2, 'Pedro Fernández', 'amgamaza.at@gmail.com', 0),
(3, 'Lucas Martínez', 'angelmanuelgamaza@gmail.com', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esper_epl_event_patterns`
--

CREATE TABLE `esper_epl_event_patterns` (
  `id` int(4) NOT NULL,
  `name` varchar(64) NOT NULL,
  `content` text NOT NULL,
  `is_deployed` tinyint(1) NOT NULL DEFAULT '0',
  `is_in_esper` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `esper_epl_event_patterns`
--

INSERT INTO `esper_epl_event_patterns` (`id`, `name`, `content`, `is_deployed`, `is_in_esper`) VALUES
(1, 'AirOzone2', '@Name("AirOzone2") insert into AirSensors select 	2 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((O3 >= 0.055 and O3 <= 0.070))].win:time(8 hour)', 1, 1),
(2, 'AirOzone3-1', '@Name("AirOzone3-1") insert into AirSensors select 	3 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((O3 >= 0.125 and O3 <= 0.164))].win:time(1 hour)', 0, 0),
(3, 'AirOzone3-8', '@Name("AirOzone3-8") insert into AirSensors select 	3 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((O3 >= 0.071 and O3 <= 0.085))].win:time(8 hour)', 0, 0),
(4, 'AirOzone4-1', '@Name("AirOzone4-1") insert into AirSensors select 	4 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((O3 >= 0.165 and O3 <= 0.204))].win:time(1 hour)', 1, 1),
(5, 'AirOzone4-8', '@Name("AirOzone4-8") insert into AirSensors select 	4 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((O3 >= 0.086 and O3 <= 0.105))].win:time(8 hour)', 0, 0),
(6, 'AirOzone5-1', '@Name("AirOzone5-1") insert into AirSensors select 	5 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((O3 >= 0.205 and O3 <= 0.404))].win:time(1 hour)', 0, 0),
(7, 'AirOzone5-8', '@Name("AirOzone5-8") insert into AirSensors select 	5 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((O3 >= 0.106 and O3 <= 0.200))].win:time(8 hour)', 0, 0),
(8, 'AirOzone6-1', '@Name("AirOzone6-1") insert into AirSensors select 	6 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((O3 >= 0.405 and O3 <= 0.604))].win:time(1 hour)', 0, 0),
(9, 'AirPM25-2', '@Name("AirPM25-2") insert into AirSensors select 	2 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM25 >= 12.1 and PM25 <= 35.4))].win:time(24 hour)', 0, 0),
(10, 'AirPM25-3', '@Name("AirPM25-3") insert into AirSensors select 	3 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM25 >= 35.5 and PM25 <= 55.4))].win:time(24 hour)', 0, 0),
(11, 'AirPM25-4', '@Name("AirPM25-4") insert into AirSensors select 	4 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM25 >= 55.5 and PM25 <= 150.4))].win:time(24 hour)', 0, 0),
(12, 'AirPM25-5', '@Name("AirPM25-5") insert into AirSensors select 	5 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM25 >= 150.5 and PM25 <= 250.4))].win:time(24 hour)', 0, 0),
(13, 'AirPM25-6', '@Name("AirPM25-6") insert into AirSensors select 	6 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM25 >= 250.5 and PM25 <= 500.4))].win:time(24 hour)', 0, 0),
(14, 'AirPM10-2', '@Name("AirPM10-2") insert into AirSensors select 	2 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM10 >= 55 and PM10 <= 154))].win:time(24 hour)', 0, 0),
(15, 'AirPM10-3', '@Name("AirPM10-3") insert into AirSensors select 	3 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM10 >= 155 and PM10 <= 254))].win:time(24 hour)', 0, 0),
(16, 'AirPM10-4', '@Name("AirPM10-4") insert into AirSensors select 	4 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM10 >= 255 and PM10 <= 354))].win:time(24 hour)', 0, 0),
(17, 'AirPM10-5', '@Name("AirPM10-5") insert into AirSensors select 	5 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM10 >= 355 and PM10 <= 424))].win:time(24 hour)', 0, 0),
(18, 'AirPM10-6', '@Name("AirPM10-6") insert into AirSensors select 	6 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((PM10 >= 425 and PM10 <= 604))].win:time(24 hour)', 0, 0),
(19, 'AirCO-3', '@Name("AirCO-3") insert into AirSensors select 	3 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((CO >= 9.5 and CO <= 12.4))].win:time(8 hour)', 0, 0),
(20, 'AirCO-4', '@Name("AirCO-4") insert into AirSensors select 	4 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((CO >= 12.5 and CO <= 15.4))].win:time(8 hour)', 1, 1),
(21, 'AirCO-5', '@Name("AirCO-5") insert into AirSensors select 	5 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((CO >= 15.5 and CO <= 30.4))].win:time(8 hour)', 0, 0),
(22, 'AirCO-6', '@Name("AirCO-6") insert into AirSensors select 	6 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((CO >= 30.5 and CO <= 50.4))].win:time(8 hour)', 0, 0),
(23, 'AirSO2-3', '@Name("AirSO2-3") insert into AirSensors select 	3 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((SO2 >= 76 and SO2 <= 185))].win:time(1 hour)', 0, 0),
(24, 'AirSO2-4', '@Name("AirSO2-4") insert into AirSensors select 	4 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((SO2 >= 186 and SO2 <= 304))].win:time(1 hour)', 0, 0),
(25, 'AirSO2-5', '@Name("AirSO2-5") insert into AirSensors select 	5 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((SO2 >= 305 and SO2 <= 604))].win:time(24 hour)', 0, 0),
(26, 'AirSO2-6', '@Name("AirSO2-6") insert into AirSensors select 	6 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((SO2 >= 605 and SO2 <= 1004))].win:time(24 hour)', 0, 0),
(27, 'AirNO2-3', '@Name("AirNO2-3") insert into AirSensors select 	3 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((NO2 >= 101 and NO2 <= 360))].win:time(1 hour)', 0, 0),
(28, 'AirNO2-4', '@Name("AirNO2-4") insert into AirSensors select 	4 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((NO2 >= 361 and NO2 <= 649))].win:time(1 hour)', 0, 0),
(29, 'AirNO2-5', '@Name("AirNO2-5") insert into AirSensors select 	5 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((NO2 >= 650 and NO2 <= 1249))].win:time(1 hour)', 1, 1),
(30, 'AirNO2-6', '@Name("AirNO2-6") insert into AirSensors select 	6 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((NO2 >= 1250 and NO2 <= 2049))].win:time(1 hour)', 0, 0),
(31, 'AirT-1', '@Name("AirT-1") insert into AirSensors select 	1 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((T <= 10))].win:time(8 hour)', 0, 0),
(32, 'AirT-6', '@Name("AirT-6") insert into AirSensors select 	6 as level, "Air" as property, 	e.id as id, e.name as name, 	e.O3 as O3, e.PM25 as PM25, e.PM10 as PM10, e.CO as CO, e.SO2 as SO2, e.NO2 as NO2, e.T as T from 	pattern [every-distinct (e.id)e = AirSensors((T >= 40))].win:time(8 hour)', 0, 0),
(33, 'WaterPH-3', '@Name("WaterPH-3") insert into WaterSensors select 	3 as level, "Water" as property, 	e.id as id, e.name as name, 	e.PH as PH, e.Cl as Cl from 	pattern [every-distinct (e.id)e =  WaterSensors((PH > 7))].win:time(1 hour)', 1, 1),
(34, 'WaterCl-3', '@Name("WaterCl-3") insert into WaterSensors select 	3 as level, "Water" as property, 	e.id as id, e.name as name, 	e.PH as PH, e.Cl as Cl from 	pattern [every-distinct (e.id)e =  WaterSensors((Cl > 2))].win:time(1 hour)', 1, 1),
(35, 'MQTT-1', '@Name("MQTT-1") insert into MQTT select 	1 as level, "Water" as property, 	e.name as name, 	e.PH as PH, e.Cl as Cl from 	pattern [every-distinct (e.name)e = MQTT((Cl > 2))].win:time(1 hour)', 0, 0),
(36, 'JuntaAndalucia-6', '@Name("JuntaAndalucia-6") insert into JuntaAndalucia select 	6 as level, "Air" as property, 	e.analizador as analizador, e.valor as valor, e.unidad as unidad from 	pattern [every-distinct (e.analizador)e = JuntaAndalucia((valor >= "40.5" and valor <= "60.4"))].win:time(1 hour)', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esper_event_types`
--

CREATE TABLE `esper_event_types` (
  `id` int(4) NOT NULL,
  `channel_id` int(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `esper_event_types`
--

INSERT INTO `esper_event_types` (`id`, `channel_id`, `name`, `description`, `is_enabled`) VALUES
(1, 170892, 'AirSensors', 'Sensors that are in Gamaza\'s home.', 0),
(2, 545022, 'WaterSensors', 'Sensors that are in Gamaza\'s beach.', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esper_event_type_epl_patterns`
--

CREATE TABLE `esper_event_type_epl_patterns` (
  `id` int(4) NOT NULL,
  `event_type_id` int(4) NOT NULL,
  `epl_pattern_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `esper_event_type_epl_patterns`
--

INSERT INTO `esper_event_type_epl_patterns` (`id`, `event_type_id`, `epl_pattern_id`) VALUES
(101, 1, 1),
(102, 1, 2),
(103, 1, 3),
(104, 1, 4),
(105, 1, 5),
(106, 1, 6),
(107, 1, 7),
(108, 1, 8),
(109, 1, 9),
(110, 1, 10),
(111, 1, 11),
(112, 1, 12),
(113, 1, 13),
(114, 1, 14),
(115, 1, 15),
(116, 1, 16),
(117, 1, 17),
(118, 1, 18),
(119, 1, 19),
(120, 1, 20),
(121, 1, 21),
(122, 1, 22),
(123, 1, 23),
(124, 1, 24),
(125, 1, 25),
(126, 1, 26),
(127, 1, 27),
(128, 1, 28),
(129, 1, 29),
(130, 1, 30),
(131, 1, 31),
(132, 1, 32),
(133, 2, 33),
(134, 2, 34);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ldap_roles`
--

CREATE TABLE `ldap_roles` (
  `id` int(4) NOT NULL,
  `role` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ldap_roles`
--

INSERT INTO `ldap_roles` (`id`, `role`) VALUES
(4, 'ROLE_ADMIN'),
(2, 'ROLE_ALERTS'),
(1, 'ROLE_ESPER'),
(7, 'ROLE_MONGO'),
(3, 'ROLE_SECURITY'),
(5, 'ROLE_SUPERADMIN'),
(6, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ldap_users`
--

CREATE TABLE `ldap_users` (
  `id` int(4) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ldap_users`
--

INSERT INTO `ldap_users` (`id`, `username`, `password`, `is_enabled`) VALUES
(1, 'esper', '$2a$10$/6jFRoolaE.eFVQkWu0ohepUMT4qMumi4gZ9.V6fX8oAh.sVQCNQi', 1),
(2, 'alerts', '$2a$10$/6jFRoolaE.eFVQkWu0ohepUMT4qMumi4gZ9.V6fX8oAh.sVQCNQi', 1),
(3, 'dba', '$2a$10$/6jFRoolaE.eFVQkWu0ohepUMT4qMumi4gZ9.V6fX8oAh.sVQCNQi', 1),
(4, 'admin', '$2a$10$/6jFRoolaE.eFVQkWu0ohepUMT4qMumi4gZ9.V6fX8oAh.sVQCNQi', 0),
(5, 'superadmin', '$2a$10$/6jFRoolaE.eFVQkWu0ohepUMT4qMumi4gZ9.V6fX8oAh.sVQCNQi', 0),
(6, 'user', '$2a$10$/6jFRoolaE.eFVQkWu0ohepUMT4qMumi4gZ9.V6fX8oAh.sVQCNQi', 0),
(7, 'mongo', '$2a$10$/6jFRoolaE.eFVQkWu0ohepUMT4qMumi4gZ9.V6fX8oAh.sVQCNQi', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ldap_user_roles`
--

CREATE TABLE `ldap_user_roles` (
  `id` int(4) NOT NULL,
  `user_id` int(4) NOT NULL,
  `role_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ldap_user_roles`
--

INSERT INTO `ldap_user_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alerts_levels`
--
ALTER TABLE `alerts_levels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level` (`level`,`property`);

--
-- Indices de la tabla `alerts_levels_users`
--
ALTER TABLE `alerts_levels_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level_id` (`level_id`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `alerts_users`
--
ALTER TABLE `alerts_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `esper_epl_event_patterns`
--
ALTER TABLE `esper_epl_event_patterns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `esper_event_types`
--
ALTER TABLE `esper_event_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `channel_id` (`channel_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `esper_event_type_epl_patterns`
--
ALTER TABLE `esper_event_type_epl_patterns`
  ADD PRIMARY KEY (`id`,`event_type_id`,`epl_pattern_id`),
  ADD KEY `event_type_id` (`event_type_id`),
  ADD KEY `epl_pattern_id` (`epl_pattern_id`);

--
-- Indices de la tabla `ldap_roles`
--
ALTER TABLE `ldap_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`role`);

--
-- Indices de la tabla `ldap_users`
--
ALTER TABLE `ldap_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `ldap_user_roles`
--
ALTER TABLE `ldap_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_index` (`user_id`,`role_id`),
  ADD KEY `ldap_ur_ibfk_2` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alerts_levels`
--
ALTER TABLE `alerts_levels`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `alerts_levels_users`
--
ALTER TABLE `alerts_levels_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `alerts_users`
--
ALTER TABLE `alerts_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `esper_epl_event_patterns`
--
ALTER TABLE `esper_epl_event_patterns`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `esper_event_types`
--
ALTER TABLE `esper_event_types`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `esper_event_type_epl_patterns`
--
ALTER TABLE `esper_event_type_epl_patterns`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT de la tabla `ldap_roles`
--
ALTER TABLE `ldap_roles`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `ldap_users`
--
ALTER TABLE `ldap_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `ldap_user_roles`
--
ALTER TABLE `ldap_user_roles`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alerts_levels_users`
--
ALTER TABLE `alerts_levels_users`
  ADD CONSTRAINT `alerts_levels_users_ibfk_1` FOREIGN KEY (`level_id`) REFERENCES `alerts_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alerts_levels_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `alerts_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `esper_event_type_epl_patterns`
--
ALTER TABLE `esper_event_type_epl_patterns`
  ADD CONSTRAINT `esper_event_type_epl_patterns_ibfk_1` FOREIGN KEY (`event_type_id`) REFERENCES `esper_event_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `esper_event_type_epl_patterns_ibfk_2` FOREIGN KEY (`epl_pattern_id`) REFERENCES `esper_epl_event_patterns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ldap_user_roles`
--
ALTER TABLE `ldap_user_roles`
  ADD CONSTRAINT `ldap_user_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ldap_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ldap_user_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ldap_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
