package com.gamaza.tfgamaza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TfgamazaApplication {
	public static void main(String[] args) {
		SpringApplication.run(TfgamazaApplication.class, args);
	}
}
