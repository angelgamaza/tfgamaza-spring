package com.gamaza.tfgamaza.mongo.controller;

import com.gamaza.tfgamaza.mongo.dto.EventTypeMongoDto;
import com.gamaza.tfgamaza.mongo.service.EventTypeMongoService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Event Types (Mongo version) Controller
 */
@RestController
@RequestMapping(value = "/mongo/event_types")
@Api(tags = "1. Event Types (Mongo)", description = "Summary of Event Types (Mongo)")
public class EventTypeMongoController {

    /* Private variables for injection */
    private final EventTypeMongoService eventTypeMongoService;

    /**
     * Constructor injection
     * @param eventTypeMongoService **eventTypeMongoService**
     */
    public EventTypeMongoController(final EventTypeMongoService eventTypeMongoService){
        this.eventTypeMongoService = eventTypeMongoService;
    }

    @PostMapping
    public @ResponseBody EventTypeMongoDto insert(@RequestBody EventTypeMongoDto eventTypeMongoDto){
        return eventTypeMongoService.create(eventTypeMongoDto);
    }

    @GetMapping
    public @ResponseBody List<EventTypeMongoDto> all(){
        return eventTypeMongoService.readAll();
    }

    @GetMapping(value = "/{id}")
    public @ResponseBody EventTypeMongoDto oneById(@PathVariable("id") String id){
        return eventTypeMongoService.readOneById(id);
    }

    @GetMapping(value = "/last")
    public @ResponseBody List<EventTypeMongoDto> last5(){
        return eventTypeMongoService.readLast5();
    }

    @DeleteMapping
    public void deleteAll(){
        eventTypeMongoService.deleteAll();
    }

    @DeleteMapping(value = "/{id}")
    public void deleteOne(@PathVariable("id") String id){
        eventTypeMongoService.deleteOne(id);
    }

}
