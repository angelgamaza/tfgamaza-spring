package com.gamaza.tfgamaza.mongo.dto;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Complex Events Detected (Mongo version) DTO
 */
@SuppressWarnings("NullableProblems")
@Getter @Setter @EqualsAndHashCode @ToString
@ApiModel(value = "3. Complex Event Detected")
public class ComplexEventDetectedMongoDto {

    private String id;

    @NotNull
    private String detectedBy;

    @NotNull
    private String detectedEvent;

    @NotNull
    private String insertionDate;

}
