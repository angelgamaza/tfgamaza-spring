package com.gamaza.tfgamaza.mongo.dto;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * EPL Event Patterns (Mongo version) DTO
 */
@SuppressWarnings("NullableProblems")
@Getter @Setter @EqualsAndHashCode @ToString
@ApiModel(value = "2. EPL Event Pattern (Mongo)")
public class EplEventPatternMongoDto {

    private String id;

    @NotNull
    private String name;

    @NotNull
    private String content;

    @NotNull
    private String insertionDate;

}
