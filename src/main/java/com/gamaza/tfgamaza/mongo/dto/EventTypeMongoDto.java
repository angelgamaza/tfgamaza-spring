package com.gamaza.tfgamaza.mongo.dto;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Event Types (Mongo version) DTO
 */
@SuppressWarnings("NullableProblems")
@Getter @Setter @EqualsAndHashCode @ToString
@ApiModel(value = "1. Event Type (Mongo)")
public class EventTypeMongoDto {

    private String id;

    @NotNull
    private String name;

    @NotNull
    private String content;

    @NotNull
    private String insertionDate;

}
