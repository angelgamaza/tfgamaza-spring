package com.gamaza.tfgamaza.mysql.alerts.controller;

import com.gamaza.tfgamaza.mysql.alerts.dto.LevelWithListDto;
import com.gamaza.tfgamaza.mysql.alerts.service.LevelService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Level Controller
 */
@RestController
@RequestMapping(value = "/alerts/levels")
@Api(tags = "1. Alert Levels", description = "Summary of Alert Levels")
public class LevelController {

    /* Private variables for injection */
    private final LevelService levelService;

    /**
     * Constructor injection
     * @param levelService **levelService**
     */
    public LevelController(final LevelService levelService){
        this.levelService = levelService;
    }

    @PostMapping
    public @ResponseBody LevelWithListDto insert(@RequestBody LevelWithListDto levelWithListDto){
        return levelService.create(levelWithListDto);
    }

    @GetMapping
    public @ResponseBody List<LevelWithListDto> all(){
        return levelService.readAll();
    }

    @GetMapping(value = "/{id}")
    public @ResponseBody LevelWithListDto oneById(@PathVariable("id") Integer id){
        return levelService.readOneById(id);
    }

    @GetMapping(value = "/property/{property}")
    public @ResponseBody List<LevelWithListDto> oneByLevel(@PathVariable("property") String property){
        return levelService.readAllByProperty(property);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") Integer id, @RequestBody LevelWithListDto levelWithListDto){
        levelService.update(id, levelWithListDto);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        levelService.delete(id);
    }

}
