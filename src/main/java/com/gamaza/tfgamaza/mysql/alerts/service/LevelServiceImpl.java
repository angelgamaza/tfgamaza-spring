package com.gamaza.tfgamaza.mysql.alerts.service;

import com.gamaza.tfgamaza.mysql.alerts.dao.LevelDao;
import com.gamaza.tfgamaza.mysql.alerts.dto.LevelWithListDto;
import com.gamaza.tfgamaza.mysql.alerts.model.Level;
import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Level Service Implementation
 */
@Service
public class LevelServiceImpl implements LevelService {

    /* Private variables for injection */
    private final LevelDao levelDao;
    private final DozerBeanMapper dozerBeanMapper;

    /**
     * Constructor injection
     * @param levelDao **levelDao**
     * @param dozerBeanMapper **dozerBeanMapper**
     */
    public LevelServiceImpl(final LevelDao levelDao, final DozerBeanMapper dozerBeanMapper){
        this.levelDao = levelDao;
        this.dozerBeanMapper = dozerBeanMapper;
    }

    @Override
    public LevelWithListDto create(LevelWithListDto levelWithListDto) {
        return dozerBeanMapper.map(levelDao.save(dozerBeanMapper.map(levelWithListDto, Level.class)), LevelWithListDto.class);
    }

    @Override
    public List<LevelWithListDto> readAll() {
        return Lists.newArrayList(levelDao.findAll()).stream().map(level -> dozerBeanMapper.map(level, LevelWithListDto.class)).collect(Collectors.toList());
    }

    @Override
    public LevelWithListDto readOneById(Integer id) {
        return dozerBeanMapper.map(levelDao.findById(id).orElseGet(Level::new), LevelWithListDto.class);
    }

    @Override
    public List<LevelWithListDto> readAllByProperty(String property) {
        return Lists.newArrayList(levelDao.findByProperty(property)).stream().map(level -> dozerBeanMapper.map(level, LevelWithListDto.class)).collect(Collectors.toList());
    }

    @Override
    public void update(Integer id, LevelWithListDto levelWithListDto) {
        levelDao.findById(id).ifPresent(level -> {
            levelWithListDto.setId(id);
            levelDao.save(dozerBeanMapper.map(levelWithListDto, Level.class));
        });
    }

    @Override
    public void delete(Integer id) {
        levelDao.deleteById(id);
    }

}
