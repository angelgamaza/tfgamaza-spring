package com.gamaza.tfgamaza.mysql.alerts.dto;


import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * User DTO (With Level List)
 */
@SuppressWarnings("NullableProblems")
@Getter @Setter @EqualsAndHashCode @ToString
@ApiModel(value = "4. Users with associated levels")
public class UserWithListDto {

    private Integer id;

    @NotNull
    private String name;

    @NotNull
    private String email;

    private boolean isEnabled;

    @NotNull
    private List<LevelDto> levels = new ArrayList<>();

}
