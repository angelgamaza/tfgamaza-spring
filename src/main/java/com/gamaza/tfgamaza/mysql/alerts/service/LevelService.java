package com.gamaza.tfgamaza.mysql.alerts.service;

import com.gamaza.tfgamaza.mysql.alerts.dto.LevelWithListDto;

import java.util.List;

/**
 * Level Service Interface
 */
public interface LevelService {

    /**
     * Insert a new Level in database
     * @param levelWithListDto **levelWithListDto**
     * @return level created
     */
    LevelWithListDto create(LevelWithListDto levelWithListDto);

    /**
     * Get all Levels in database
     * @return levels list
     */
    List<LevelWithListDto> readAll();

    /**
     * Search and return one Level by id
     * @param id **id**
     * @return level found
     */
    LevelWithListDto readOneById(Integer id);

    /**
     * Get all Levels by property
     * @param property **property**
     * @return level list
     */
    List<LevelWithListDto> readAllByProperty(String property);

    /**
     * Modify one Level in database
     * @param id **id**
     * @param levelWithListDto **levelWithListDto**
     */
    void update(Integer id, LevelWithListDto levelWithListDto);

    /**
     * Delete one Level in database
     * @param id **id**
     */
    void delete(Integer id);

}
