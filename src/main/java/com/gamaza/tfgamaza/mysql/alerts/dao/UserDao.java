package com.gamaza.tfgamaza.mysql.alerts.dao;

import com.gamaza.tfgamaza.mysql.alerts.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * User DAO
 */
@Repository
public interface UserDao extends CrudRepository<User, Integer> {

    /**
     * Search Users in database by email
     * @param email **email**
     * @return user found
     */
    Optional<User> findByEmail(String email);

    /**
     * Get all enabled/disabled Users in database
     * @param status **status**
     * @return user enabled/disabled list
     */
    @SuppressWarnings("SpringDataMethodInconsistencyInspection")
    List<User> findAllByIsEnabled(boolean status);

}
