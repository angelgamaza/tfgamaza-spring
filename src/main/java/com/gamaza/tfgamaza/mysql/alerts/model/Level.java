package com.gamaza.tfgamaza.mysql.alerts.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Level Model
 */
@Entity
@Table(name = "alerts_levels")
@Getter @Setter @EqualsAndHashCode
public class Level {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "level", nullable = false, unique = true)
    private Integer level;

    @Column(name = "property", nullable = false)
    private String property;

    @Column(name = "advices", nullable = false)
    private String advices;

    @Column(name = "notification_color", nullable = false)
    private String notificationColor;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "alerts_levels_users", joinColumns = {@JoinColumn(name = "level_id")}, inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private List<User> users;

}
