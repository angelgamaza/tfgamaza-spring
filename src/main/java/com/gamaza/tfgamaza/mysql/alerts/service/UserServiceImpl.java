package com.gamaza.tfgamaza.mysql.alerts.service;

import com.gamaza.tfgamaza.mysql.alerts.dao.LevelDao;
import com.gamaza.tfgamaza.mysql.alerts.dao.UserDao;
import com.gamaza.tfgamaza.mysql.alerts.dto.UserWithListDto;
import com.gamaza.tfgamaza.mysql.alerts.exception.SubscribeException;
import com.gamaza.tfgamaza.mysql.alerts.model.User;
import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * User Service Implementation
 */
@Service
public class UserServiceImpl implements UserService{

    /* Private variables for injection */
    private final UserDao userDao;
    private final LevelDao levelDao;
    private final DozerBeanMapper dozerBeanMapper;

    /**
     * Constructor injection
     * @param userDao **userDao**
     * @param levelDao **levelDao**
     * @param dozerBeanMapper **dozerBeanMapper**
     */
    public UserServiceImpl(final UserDao userDao, final LevelDao levelDao, final DozerBeanMapper dozerBeanMapper){
        this.userDao = userDao;
        this.levelDao = levelDao;
        this.dozerBeanMapper = dozerBeanMapper;
    }

    @Override
    public UserWithListDto create(UserWithListDto userWithListDto) {
        return dozerBeanMapper.map(userDao.save(dozerBeanMapper.map(userWithListDto, User.class)), UserWithListDto.class);
    }

    @Override
    public List<UserWithListDto> readAll() {
        return Lists.newArrayList(userDao.findAll()).stream().map(user -> dozerBeanMapper.map(user, UserWithListDto.class)).collect(Collectors.toList());
    }

    @Override
    public List<UserWithListDto> readAllByIsEnabled(boolean status) {
        return Lists.newArrayList(userDao.findAllByIsEnabled(status)).stream().map(user -> dozerBeanMapper.map(user, UserWithListDto.class)).collect(Collectors.toList());
    }

    @Override
    public UserWithListDto readOneById(Integer id) {
        return dozerBeanMapper.map(userDao.findById(id).orElseGet(User::new), UserWithListDto.class);
    }

    @Override
    public UserWithListDto readOneByEmail(String email) {
        return dozerBeanMapper.map(userDao.findByEmail(email).orElseGet(User::new), UserWithListDto.class);
    }

    @Override
    public void update(Integer id, UserWithListDto userWithListDto) {
        userDao.findById(id).ifPresent(user -> {
            userWithListDto.setId(id);
            userDao.save(dozerBeanMapper.map(userWithListDto, User.class));
        });
    }

    @Override
    public void subscribe(Integer userId, Integer levelId) {
        userDao.findById(userId).ifPresent(user -> levelDao.findById(levelId).ifPresent(level -> {
            //Check if user is subscribed to the level
            if(user.getLevels().contains(level))
                throw new SubscribeException("User already subscribed to this level");
            else {
                user.getLevels().add(level);
                level.getUsers().add(user);
            }
            //Update in database
            userDao.save(user);
            levelDao.save(level);
        }));
    }

    @Override
    public void unsubscribe(Integer userId, Integer levelId) {
        userDao.findById(userId).ifPresent(user -> levelDao.findById(levelId).ifPresent(level -> {
            //Delete only if exists
            user.getLevels().removeIf(element -> element.equals(level));
            level.getUsers().removeIf(element -> element.equals(user));
            //Update in database
            userDao.save(user);
            levelDao.save(level);
        }));
    }

    @Override
    public void delete(Integer id) {
        userDao.deleteById(id);
    }

}
