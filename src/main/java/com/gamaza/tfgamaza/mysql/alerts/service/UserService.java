package com.gamaza.tfgamaza.mysql.alerts.service;

import com.gamaza.tfgamaza.mysql.alerts.dto.UserWithListDto;

import java.util.List;

/**
 * User Service Interface
 */
public interface UserService {

    /**
     * Insert a new user in database
     * @param userWithListDto **userWithListDto**
     * @return user created
     */
    UserWithListDto create(UserWithListDto userWithListDto);

    /**
     * Get all Users in database
     * @return users list
     */
    List<UserWithListDto> readAll();

    /**
     * Get all enabled/disabled Users in database
     * @param status **status**
     * @return user enabled/disabled list
     */
    List<UserWithListDto> readAllByIsEnabled(boolean status);

    /**
     * Search and return one User by id
     * @param id **id**
     * @return user found
     */
    UserWithListDto readOneById(Integer id);

    /**
     * Search and return one User by email
     * @param email **email**
     * @return user found
     */
    UserWithListDto readOneByEmail(String email);

    /**
     * Modify one User in database
     * @param id **id**
     * @param userWithListDto **userWithListDto**
     */
    void update(Integer id, UserWithListDto userWithListDto);

    /**
     * Subscribe User to Level
     * @param userId **userId**
     * @param levelId **levelId**
     */
    void subscribe(Integer userId, Integer levelId);

    /**
     * Unsubscribe User to Level
     * @param userId **userId**
     * @param levelId **levelId**
     */
    void unsubscribe(Integer userId, Integer levelId);

    /**
     * Delete one User in database
     * @param id **id**
     */
    void delete(Integer id);


}
