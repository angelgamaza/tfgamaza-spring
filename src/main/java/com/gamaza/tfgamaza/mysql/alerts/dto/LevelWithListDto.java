package com.gamaza.tfgamaza.mysql.alerts.dto;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Level DTO (With Users List)
 */
@SuppressWarnings("NullableProblems")
@Getter @Setter @EqualsAndHashCode @ToString
@ApiModel(value = "2. Levels with associated users")
public class LevelWithListDto {

    private Integer id;

    @NotNull
    private Integer level;

    @NotNull
    private String advices;

    @NotNull
    private String property;

    @NotNull
    private String notificationColor;

    @NotNull
    private List<UserDto> users = new ArrayList<>();

}
