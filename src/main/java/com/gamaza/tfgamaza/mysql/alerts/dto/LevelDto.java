package com.gamaza.tfgamaza.mysql.alerts.dto;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Level DTO
 */
@SuppressWarnings({"NullableProblems", "WeakerAccess"})
@Getter @Setter @EqualsAndHashCode @ToString
@ApiModel(value = "1. Level")
public class LevelDto {

    private Integer id;

    @NotNull
    private Integer level;

    @NotNull
    private String property;

    @NotNull
    private String advices;

    @NotNull
    private String notificationColor;

}
