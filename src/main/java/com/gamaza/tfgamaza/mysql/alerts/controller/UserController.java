package com.gamaza.tfgamaza.mysql.alerts.controller;

import com.gamaza.tfgamaza.mysql.alerts.dto.UserWithListDto;
import com.gamaza.tfgamaza.mysql.alerts.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * User Controller
 */
@RestController
@RequestMapping(value = "/alerts/users")
@Api(tags = "2. Alert Users", description = "Summary of Users")
public class UserController {

    /* Private variables for injection */
    private final UserService userService;

    /**
     * Constructor injection
     * @param userService **userService**
     */
    public UserController(final UserService userService){
        this.userService = userService;
    }

    @PostMapping 
    public @ResponseBody UserWithListDto insert(@RequestBody UserWithListDto userWithListDto){
        return userService.create(userWithListDto);
    }

    @GetMapping
    public @ResponseBody List<UserWithListDto> all(){
        return userService.readAll();
    }

    @GetMapping(value = "/enabled")
    public @ResponseBody List<UserWithListDto> allEnabled(){
        return userService.readAllByIsEnabled(true);
    }

    @GetMapping(value = "/disabled")
    public @ResponseBody List<UserWithListDto> allDisabled(){
        return userService.readAllByIsEnabled(false);
    }

    @GetMapping(value = "/{id}")
    public @ResponseBody UserWithListDto oneById(@PathVariable("id") Integer id){
        return userService.readOneById(id);
    }

    @GetMapping(value = "/email/{email}")
    public @ResponseBody UserWithListDto oneByLevel(@PathVariable("email") String email){
        return userService.readOneByEmail(email);
    }

    @GetMapping(value = "/subscribe/{userId}/{levelId}")
    public void subscribe(@PathVariable("userId") Integer userId, @PathVariable("levelId") Integer levelId){
        userService.subscribe(userId, levelId);
    }

    @GetMapping(value = "/unsubscribe/{userId}/{levelId}")
    public void unsubscribe(@PathVariable("userId") Integer userId, @PathVariable("levelId") Integer levelId){
        userService.unsubscribe(userId, levelId);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") Integer id, @RequestBody UserWithListDto userWithListDto){
        userService.update(id, userWithListDto);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        userService.delete(id);
    }


}
