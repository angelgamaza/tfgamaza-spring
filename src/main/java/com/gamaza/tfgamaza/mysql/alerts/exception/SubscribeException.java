package com.gamaza.tfgamaza.mysql.alerts.exception;

/**
 * SubscribeException class
 */
public class SubscribeException extends RuntimeException {
    public SubscribeException(String message){ super(message); }
}
