package com.gamaza.tfgamaza.mysql.alerts.dto;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * User DTO
 */
@SuppressWarnings({"NullableProblems", "WeakerAccess"})
@Getter @Setter @EqualsAndHashCode @ToString
@ApiModel(value = "3. User")
public class UserDto {

    private Integer id;

    @NotNull
    private String name;

    @NotNull
    private String email;

    private boolean isEnabled;

}
