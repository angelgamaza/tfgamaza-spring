package com.gamaza.tfgamaza.mysql.alerts.dao;

import com.gamaza.tfgamaza.mysql.alerts.model.Level;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Level DAO
 */
@Repository
public interface LevelDao extends CrudRepository<Level, Integer> {

    /**
     * Search Levels in database by property
     * @param property **property**
     * @return level found
     */
    List<Level> findByProperty(String property);

}
