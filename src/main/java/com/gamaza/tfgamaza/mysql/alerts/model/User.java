package com.gamaza.tfgamaza.mysql.alerts.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * User Model
 */
@Entity
@Table(name = "alerts_users")
@Getter @Setter @EqualsAndHashCode
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "is_enabled", nullable = false)
    private boolean isEnabled;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "users")
    private List<Level> levels;

}
