package com.gamaza.tfgamaza.mysql.tools;

import io.swagger.annotations.Api;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

/**
 * Little API method for encode passwords
 */
@RestController
@RequestMapping("/security")
@Api(tags = "1. Password Generator", description = "Password generator method for Spring Security")
public class PasswordGeneratorController {

    @GetMapping(value = "/generate_password/{password}")
    public @ResponseBody String generatePassword(@PathVariable("password") String password){
        return new BCryptPasswordEncoder().encode(password);
    }

}
