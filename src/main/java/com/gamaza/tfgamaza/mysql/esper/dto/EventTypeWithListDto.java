package com.gamaza.tfgamaza.mysql.esper.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Event Types DTO (With EPL Event Pattern List)
 */
@SuppressWarnings("NullableProblems")
@Getter @Setter @ToString
@ApiModel(value = "2. Event Type with EPL Event Patterns associated")
public class EventTypeWithListDto {

    private Integer id;

    @NotNull
    private Integer channel;

    @NotNull
    private String name;

    @NotNull
    private String description = "No description";

    private boolean isEnabled = false;

    @NotNull
    private List<EplEventPatternDto> eplEventPatterns = new ArrayList<>();

}

