package com.gamaza.tfgamaza.mysql.esper.exception;

/**
 * EventTypeException class
 */
public class EventTypeException extends RuntimeException {
    public EventTypeException(String message){
        super(message);
    }
}
