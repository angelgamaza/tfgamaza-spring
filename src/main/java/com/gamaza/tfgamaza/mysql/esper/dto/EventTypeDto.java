package com.gamaza.tfgamaza.mysql.esper.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Event Types DTO
 */
@SuppressWarnings({"NullableProblems", "WeakerAccess"})
@Getter @Setter @ToString
@ApiModel(value = "1. Event Type")
public class EventTypeDto {

    private Integer id;

    @NotNull
    private Integer channel;

    @NotNull
    private String name;

    @NotNull
    private String description = "No description";

    private boolean isEnabled = false;

}
