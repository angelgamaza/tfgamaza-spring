package com.gamaza.tfgamaza.mysql.esper.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * EPL Event Patterns DTO
 */
@SuppressWarnings({"NullableProblems", "WeakerAccess"})
@Getter @Setter @ToString
@ApiModel(value = "3. EPL Event Pattern")
public class EplEventPatternDto {

    private Integer id;

    @NotNull
    private String name;

    @NotNull
    private String content;

    private boolean isDeployed = false;

    private boolean isInEsper = false;

}
