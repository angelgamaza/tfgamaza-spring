package com.gamaza.tfgamaza.mysql.esper.exception;

/**
 * EplEventPatternDeployedException class
 */
public class EplEventPatternDeployedException extends RuntimeException {
    public EplEventPatternDeployedException(String message){
        super(message);
    }
}
