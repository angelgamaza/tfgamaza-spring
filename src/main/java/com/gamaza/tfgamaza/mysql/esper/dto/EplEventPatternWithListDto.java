package com.gamaza.tfgamaza.mysql.esper.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * EPL Event Patterns DTO (With Event Types List)
 */
@SuppressWarnings("NullableProblems")
@Getter @Setter @ToString
@ApiModel(value = "4. EPL Event Pattern with Event Types associated")
public class EplEventPatternWithListDto {

    private Integer id;

    @NotNull
    private String name;

    @NotNull
    private String content;

    private boolean isDeployed = false;

    private boolean isInEsper = false;

    @NotNull
    private List<EventTypeDto> eventTypes = new ArrayList<>();

}