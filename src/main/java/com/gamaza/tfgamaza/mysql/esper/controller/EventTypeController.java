package com.gamaza.tfgamaza.mysql.esper.controller;

import com.gamaza.tfgamaza.mysql.esper.dto.EventTypeWithListDto;
import com.gamaza.tfgamaza.mysql.esper.service.EventTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Event Types Controller
 */
@RestController
@RequestMapping(value = "/esper/event_types")
@Api(tags = "1. Esper Event Types", description = "Summary of Esper Event Types")
public class EventTypeController {

    /* Private variables for injection */
    private final EventTypeService eventTypeService;

    /**
     * Constructor injection
     * @param eventTypeService **eventTypeService**
     */
    public EventTypeController(final EventTypeService eventTypeService){
        this.eventTypeService = eventTypeService;
    }

    @PostMapping
    public @ResponseBody EventTypeWithListDto insert(@RequestBody EventTypeWithListDto eventTypeWithListDto){
        return eventTypeService.create(eventTypeWithListDto);
    }

    @GetMapping
    public @ResponseBody List<EventTypeWithListDto> all(){
        return eventTypeService.readAll();
    }

    @GetMapping(value = "/enabled")
    public @ResponseBody List<EventTypeWithListDto> allEnabled(){
        return eventTypeService.readAllByIsEnabled(true);
    }

    @GetMapping(value = "/disabled")
    public @ResponseBody List<EventTypeWithListDto> allDisabled(){
        return eventTypeService.readAllByIsEnabled(false);
    }

    @GetMapping(value = "/{id}")
    public @ResponseBody EventTypeWithListDto oneById(@PathVariable("id") Integer id){
        return eventTypeService.readOneById(id);
    }

    @GetMapping(value = "/name/{name}")
    public @ResponseBody EventTypeWithListDto oneByName(@PathVariable("name") String name){
        return eventTypeService.readOneByName(name);
    }

    @GetMapping(value = "/channel/{channelId}")
    public @ResponseBody EventTypeWithListDto oneByChannelId(@PathVariable("channelId") Integer channelId){
        return eventTypeService.readOneByChannelId(channelId);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") Integer id, @RequestBody EventTypeWithListDto eventTypeWithListDto){
        eventTypeService.update(id, eventTypeWithListDto);
    }

    @GetMapping(value = "/enable/{id}")
    public void enable(@PathVariable("id") Integer id){
        eventTypeService.updateStatus(id, true);
    }

    @GetMapping(value = "/disable/{id}")
    public void disable(@PathVariable("id") Integer id){
        eventTypeService.updateStatus(id, false);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        eventTypeService.delete(id);
    }

}
