## Ángel Gamaza TFG

This is the Bitbucket repository that contains the Ángel Gamaza TFG's API REST done with Spring and more libraries like the following:

1. **Spring MVC:** Allows REST calls and method implementations.
2. **Spring Security:** Allows REST calls securization.
3. **JDBC and JPA:** Allows secured database connections and easy queries.
4. **Spring Data MongoDB:** Allows NoSQL database connections and queries.
5. **Jackson and Dozer:** Libraries that allow objects mapping.
6. **Lombok:** Library that allows easily class structures creation.
7. **Swagger:** Library to show the API in an UI.

---